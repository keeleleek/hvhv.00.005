---
references:
- id: Strupler2018
  title: Open Science Review
  author:
  - family: Strupler
	given: Néhémie
  issued:
    year: 2018
---
# Exam for Open and Collaborative Humanities (HVHV.00.005)

This is a repository where I pass the exam.

## Further things to do

Find out when and where Strupler really said, that "There is noting true if it is not transparent [@Strupler2018]".

## Illustrating Science

![xkcd: the difference](https://imgs.xkcd.com/comics/the_difference.png)

Source of image: [xkcd, The difference, CC BY-NC 2.5](https://xkcd.com/242/)


# References
