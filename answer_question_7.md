# The Citation Style Language

CSL is a language (actually an XML schema that specifies this language) that is used to _describe how citations and bibliographies are styled_. 

Factoring out the styling from citations and bibliographies management enables the writer to store all references in a central format (in other words, to use a database). For each article or text the writer then simply uses a *style* that is used to automatically format the contents from the central reference storage and renders it inside the text.

The efforts behind the Citation Style Language started in the OpenOffice community and was then adopted by Zotero and later on also by Mendeley. There exists several tools for both visually declaring a new style and for searching visually similar styles.

For using CSL with Pandoc was actually implicitly needed for answering Question 3. Pandoc uses an external tool called CiteProc for transforming the in-text citations and bibliographies according to the stated citation style. For this one needs to add the argument ``--filter pandoc-citeproc`` to the pandoc command.
