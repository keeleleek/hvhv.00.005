# What is Markdown?

Markdown is a simple markup schema to write plain text as if it was formatted enriched text.

The main strength of using Markdown (imho) is not that it is cool (i.e using Atom as editor), but that it enables basic markup of text -- paragraphs, text emphasis (bold, italics, etc) and structuring of the text into sections. And because Markdown really is simple for everyone to use it, it definitely raises 
