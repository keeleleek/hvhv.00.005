---
bibliography: bibliography.bib
---

# The main point of Fanelli 2013

The article [@fanelli_redefine_2013] states that many important possible solutions exist for protecting science from an epidemic of false, biased and falsified findings.

But the author brings out that the key to protecting science is to strengthen self-correction. "Publication, peer-review and misconduct investigations should focus less on what scientists do, and more on what they communicate."

The current situation relies on whistle-blowers who report on wrong-doings of researchers. This is insufficient because there simply is too many researchers.

Instead, each field of study should create their own "standard" guidelines for precisely what to publish (e.g. to communicate). The task of journal publishers would then be to ensure that publications really publish data according to this standard.

Then the task of matching the published facts with re-produced results could be almost automated. If not automated, then atleast undergraduates could be trained in following the guidelines and also be used as labour force in checking if publish results match the results of their re-productions.

# References

