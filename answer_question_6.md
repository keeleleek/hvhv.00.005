# What is Pandoc?

Pandoc is a system that can:

1. read in structured text (be it Markdown, Org, etc) 
1. interpret it into a semantical interlingua (which states 'this is a heading', 'this is in bold', 'this is in italics', etc)
1. output the interpretion into another structuring schema (be it Markdown, Org, HTML, ODT, LaTeX, etc)

... and of course it has a plethora of plugins for bringing stuff taken for granted in other markup schemes/traditions into the simpler ones used as input (i.e citeing Markdown) ...
