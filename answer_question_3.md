---
bibliography: bibliography.bib
---

# Main problems identified by Ioannidis et al. 2014

The article [@ioannidis_how_2014] brings out the following main problems in the context of the vast global industry that scientific research has grown to be.

* true and readily applicable major discoveries are far fewer than the output
* many of the new proposed associations and/or effects are false or grossly exaggerated
* the translation of knowledge into useful applications is often slow and potentially inefficient

The authors estimate that currently 85% of research resources are wasted.

# References

