Creative Commons is **not** a license, it is an American non-profit organization that works out a suit of licenses. The licenses regulate which rights the copyright owner (the author of the work) wants to waive, that is, to give to others, the users of the copyrighted work.

The Creative Commons licenses are not recomended to be used for software because they don't specify what rights should adhere to the underlying source code nor how it should be distributed (see [[https://creativecommons.org/faq/#can-i-apply-a-creative-commons-license-to-software]]). A "freely licensed" program without a "freely licensed" source code is not "freely licensed" at all.

Instead the Creative Commons licenses are good for artistic, cultural and educational work, including data and databases. This kind of works are different from software because they in a sense entail themselves and are at least in their most usal form not divided between a "surface form" and underlying "source code form". They can be modified by modifying them directly, instead of through their underlying source code.

The most common rights to waive are:
* the right to use the work with (or without) attribution
* the right to share the work to others
* the right to further build or add on the work

And the most common licenses that reflect the regulation of these rights are:
* CC0 *almost all* rights are given -- no need for attribution
* CC-BY only the right to use is given -- need for clear attribution
* CC-BY-SA gives the right to use and further build upon the work *if* the derived work is made available under the same license

Some examples are that Wikidedia uses CC-BY-SA for most parts of articles and CC0 for the semantic metadata. Only CC0 is recommended for software by the Free Software Foundation (the author of the GPL licenses).
